﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Binary Search Tree Node class
namespace BinarySearchTreeChallenge
{
    public class BSTNode
    {
        private int nodeValue;
        //private BSTNode parent = null;
        private BSTNode leftSibling = null;
        private BSTNode rightSibling = null;
        private int valueCount = 0;

        public BSTNode(int _value)
        {
            nodeValue = _value;
        }

        public void Insert(int _value)
        {
            if (_value == nodeValue)
            {
                _value++;
                return;
            }

            if (_value > nodeValue)
            {
                if (rightSibling == null)
                    rightSibling = new BSTNode(_value);
                rightSibling.Insert(_value);
                return;
            }

            if (_value < nodeValue)
            {
                if (leftSibling == null)
                    leftSibling = new BSTNode(_value);

                leftSibling.Insert(_value);
                return;
            }

            //check if larger or smaller
            //add to the correct side
        }

        public void Delete(int value)
        {
            //check if this is the correct node
            //if it is do all the stuff to remove it
        }

        public override string ToString()
        {
            Debug.Log("NodeValue ToString: " + nodeValue.ToString());
            string returnString = "";
            if (leftSibling == null)
                returnString += "null/";
            else
                returnString += leftSibling.ToString() + "/";

            returnString += nodeValue.ToString();

            if (rightSibling == null)
                returnString += "\\null";
            else
                returnString += "\\" + rightSibling.ToString();

            return returnString;
        }

    }

}//end of NameSpace